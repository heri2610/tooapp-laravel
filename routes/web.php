<?php

use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UsersController;
use Inertia\Inertia;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/delete/{id}', [ProductController::class, 'destroy']);
Route::get('/detail/{id}', [ProductController::class, 'show']);
Route::resource('/', ProductController::class);
Route::get('/notSold/{id}', [ProductController::class, 'NotLaku']);
Route::resource('/transaksi', InvoiceController::class);
Route::resource('/topCuntomers', UsersController::class);
// Route::resource('/', function () {
//         return Inertia::render('ProductMahal');
// });

require __DIR__.'/auth.php';
