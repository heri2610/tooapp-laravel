<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice_Detail extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public function Invoice(){
        return $this->belongsTo(Invoice::class);
    }
    public function Product(){
        return $this->belongsTo(Product::class);
    }
}
