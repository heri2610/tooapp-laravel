## Set Up

menggunakan ekosistem breeze dengan view nya/ frontEnd nya react melalui inertia

-   Pertama install vendor dengan mengetikan composer install, kemudian install node_modules dengan menetikan perintah npm install di terminal.
-   buka .env, sesuaikan konfigurasi.
-   lakukan migrasi dengan mengetik PHP artisan migrate untuk mengiisi kolom database.
-   lakukan seeder denagn perintah php artisan db:seed
-   terakhir ketikan perintah php artisan serve dan buka terminal satu lagi ketikan npm run start
