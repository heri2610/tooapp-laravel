<?php

namespace Database\Seeders;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use \App\Models\Users;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Users::factory()->create([
            'name' => 'suhaeri',
            'addres' => 'taktakan serang',
        ]);
        Users::factory()->create([
            'name' => 'mujib',
            'email' => 'muncang kopong rangkas',
        ]);
        Users::factory()->create([
            'name' => 'alvin',
            'email' => 'cikaung pandeglang',
        ]);
        Users::factory()->create([
            'name' => 'syarif',
            'email' => 'kampus mbe',
        ]);
        Users::factory()->create([
            'name' => 'tio',
            'email' => 'kresek tangerang',
        ]);
        Users::factory()->create([
            'name' => 'raped',
            'email' => 'tiga raksa tangerang',
        ]);
    }
}
