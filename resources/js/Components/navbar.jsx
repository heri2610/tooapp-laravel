export default function Navbar(props) {
    console.log(props);
    return (
        <div className="navbar bg-sky-500 text-neutral-content">
            <div className="flex-1">
                <a className="btn btn-ghost normal-case text-xl">AITINDO</a>
            </div>
            <div className="flex-none">
                <ul className="menu menu-horizontal p-0 ">
                    <li tabIndex={0}>
                        <a>
                            Todos
                            <svg
                                className="fill-current"
                                xmlns="http://www.w3.org/2000/svg"
                                width="20"
                                height="20"
                                viewBox="0 0 24 24"
                            >
                                <path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />
                            </svg>
                        </a>
                        <ul className="z-50 text-orange-600 p-2 bg-stone-400 menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52">
                            <li>
                                <a href="/" className={props.PMahal}>
                                    Product Termahal
                                </a>
                            </li>
                            <li>
                                <a
                                    href="/transaksi"
                                    className={props.transakasi}
                                >
                                    Daftar Transaksi
                                </a>
                            </li>
                            <li>
                                <a
                                    href="/topCuntomers"
                                    className={props.Cuntomers}
                                >
                                    Top Customers
                                </a>
                            </li>
                            <li>
                                <a href="/notSold" className={props.notSold}>
                                    Product Tidak Pernah Dibeli
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>Users</a>
                    </li>

                    <li>
                        <a>Product</a>
                    </li>
                </ul>
            </div>
        </div>
    );
}
