import React from "react";
import { Link, Head } from "@inertiajs/inertia-react";
import Navbar from "@/Components/navbar";
export default function Todo(props) {
    const products = props.data;
    return (
        <>
            <Head title="detail Product" />
            <Navbar />
            {products.map((product) => {
                return (
                    <>
                        <div className="card w-96 bg-base-100 shadow-xl mb4">
                            <div className="card-body">
                                <p className="card-title">{products.id}</p>
                                <p>300.0000</p>
                                <p>{product.date}</p>
                            </div>
                        </div>
                    </>
                );
            })}
        </>
    );
}
