import React from "react";
import { Link, Head } from "@inertiajs/inertia-react";
import Navbar from "@/Components/navbar";
export default function Todo(props) {
    const products = props.data;
    return (
        <div>
            <Head title="5 Product Termahal" />
            <Navbar PMahal="active" />
            <div className="px-4">
                <h1 className="font-bold text-2xl text-center my-4">
                    Daftar 5 Product Termahal
                </h1>
                <div className="overflow-x-auto w-full">
                    <table className="table w-full">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map((product) => {
                                return (
                                    <tr key={product.id}>
                                        <td>{product.name}</td>
                                        <td>
                                            <div className="flex items-center space-x-3">
                                                <div>
                                                    <div className="font-bold">
                                                        {product.name}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{product.price}</td>
                                        <th>
                                            <a
                                                href={`/detail/${product.id}`}
                                                className="btn btn-primary"
                                            >
                                                details
                                            </a>
                                            <form
                                                action={`/delete/${product.id}`}
                                                className="inline"
                                                method="post"
                                            >
                                                <button className="btn btn-error ml-2">
                                                    delete
                                                </button>
                                            </form>
                                        </th>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
