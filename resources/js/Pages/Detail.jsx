import React from "react";
import { Link, Head } from "@inertiajs/inertia-react";
import Navbar from "@/Components/navbar";
export default function Todo(props) {
    const products = props.data;
    return (
        <>
            <Head title="detail Product" />
            <Navbar />
            <div className="card w-96 bg-base-100 shadow-xl m-auto">
                <div className="card-body">
                    <h2 className="card-title">{products.name}</h2>
                    <p>{products.price}</p>
                    <div className="card-actions justify-end">
                        <button className="btn btn-primary">Buy Now</button>
                    </div>
                </div>
            </div>
        </>
    );
}
